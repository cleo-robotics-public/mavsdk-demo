import asyncio
import numpy as np
from mavsdk import System
from mavsdk.offboard import (OffboardError, PositionNedYaw)

altitude = 0

async def run():
	drone = System(mavsdk_server_address='localhost', port=50051)
	await drone.connect()

	print("Waiting for drone to connect...")
	async for state in drone.core.connection_state():
		if state.is_connected:
			print(f"-- Connected to drone!")
			break

	print("-- Getting initial position")
	async for pos_vel_ned in drone.telemetry.position_velocity_ned():
		altitude = pos_vel_ned.position.down_m
		break

	altitude_setpoint = altitude - 1.5
	altitude_error = -0.6

	print("-- Setting initial setpoint")
	await drone.offboard.set_position_ned(PositionNedYaw(0, 0, altitude_error, 0.0))

	print("-- Starting offboard")
	try:
		await drone.offboard.start()
	except OffboardError as error:
		print(f"Starting offboard mode failed with error code: {error._result.result}")
		print("-- Disarming")
		await drone.action.disarm()
		return

	print("-- Arming")
	await drone.action.arm()

	print("-- Running loop")
	while True:
		async for pos_vel_ned in drone.telemetry.position_velocity_ned():
			altitude = pos_vel_ned.position.down_m
			break

		altitude_error = max(altitude_setpoint - altitude, -0.6)
		await drone.offboard.set_position_ned(PositionNedYaw(0, 0, altitude_error, 0.0))

		if abs(altitude - altitude_setpoint) < 0.5:
			break

		await asyncio.sleep(10/1000) # 10Hz

	print("-- Landing")
	try:
		await drone.action.land()
	except ActionError as error:
		print(f"Landing failed with error code: {error._result.result}")

if __name__ == "__main__":
	loop = asyncio.get_event_loop()
	loop.run_until_complete(run())