# MAVSDK Demo

Example scripts for controlling the Dronut using the python MAVSDK library.

## Commands

The Dronut is controlled through Mavlink offboard position commands in the NED frame. The commands act as an error offset rather than absolute waypoint-like values. For example to instruct the Dronut to hover in place the position setpoints should be `0, 0, 0, 0`. To make the drone fly forward the setpoints would be `1, 0, 0, 0`, note that this will cause the Dronut to fly forward continuously until it receives all zeros; the magnitude can be thought of as the speed of the translation. The value order is North, East, Down, and Yaw with the positive axes pointing in the respective directions. In order to instruct the Dronut to fly a specific distance a simple feedback controller is required which keeps track of the position values. The script `multiple_commands.py` includes an example of such a controller.

## Dependencies
Installed using pip3:
- mavsdk
- numpy

## Usage
1. Connect a computer to the Dronut
2. Get the computer's IP address which should resemble `192.168.8.X` by running `ifconfig`
3. Modify the file `/etc/modalai/voxl-vision-px4.conf` on the Dronut and set the parameter `qgc_ip` to the IP from step 2
4. Reboot the Dronut by running `sync` to make sure the changes to the configuration file have been saved and then `reboot`
5. Run a mavsdk server on the computer: `./mavsdk_server_macos -p 50051 "udp://:14550"`. You should see the message: `New system on: 192.168.8.1:14550`
6. Run a MAVSDK script from the computer: `python3 takeoff_land.py`

## License
MIT