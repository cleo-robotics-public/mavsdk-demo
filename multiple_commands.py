import asyncio
import numpy as np
from mavsdk import System
from mavsdk.offboard import (OffboardError, PositionNedYaw)

# Error threshold for advancing to the next command
error_threshold = 0.5

# How many times to send a command per second
offboard_frequency = 5

# Offsets to send to the Dronut
input_commands = np.array([
	[0, 0, -1.5],  	# go up - takeoff
	[2, 0, 0],  	# go forward
	[0, 2, 0],  	# go right
	[-2, 0, 0],  	# go back
	[0, -2, 0]  	# go left
], dtype=float)

# Index of command being executed
command_index = 0

# Position vector from drone
position_vector = np.array([0, 0, 0], dtype=float)

# Position setpoints calculated from position telemetry and input command values
setpoint_vector = np.array([0, 0, 0], dtype=float)

# Command vector sent to the Dronut
command_vector = np.array([0, 0, 0], dtype=float)

async def run():
	global command_index

	drone = System(mavsdk_server_address='localhost', port=50051)
	await drone.connect()

	print("Waiting for drone to connect...")
	async for state in drone.core.connection_state():
		if state.is_connected:
			print(f"-- Connected to drone!")
			break

	print("-- Getting initial position")
	async for pos_vel_ned in drone.telemetry.position_velocity_ned():
		position_vector[0] = pos_vel_ned.position.north_m
		position_vector[1] = pos_vel_ned.position.east_m
		position_vector[2] = pos_vel_ned.position.down_m
		break

	# Prepare first command which is usually takeoff
	setpoint_vector = position_vector + input_commands[command_index]
	error_vector = setpoint_vector - position_vector
	error_vector[2] = -0.6 # Works best for takeoff

	print("-- Setting initial setpoint")
	await drone.offboard.set_position_ned(PositionNedYaw(
			error_vector[0], 
			error_vector[1],
			error_vector[2],
			0.0))

	print("-- Starting offboard")
	try:
		await drone.offboard.start()
	except OffboardError as error:
		print(f"Starting offboard mode failed with error code: {error._result.result}")
		print("-- Disarming")
		await drone.action.disarm()
		return

	print("-- Arming")
	await drone.action.arm()

	print("-- Running loop")
	while True:

		# Calculate error vector which is the input to the drone
		async for pos_vel_ned in drone.telemetry.position_velocity_ned():
			position_vector[0] = pos_vel_ned.position.north_m
			position_vector[1] = pos_vel_ned.position.east_m
			position_vector[2] = pos_vel_ned.position.down_m
			break

		error_vector = setpoint_vector - position_vector
		error_vector[2] = max(error_vector[2], -0.6) # Better performance at takeoff if the altitude error is capped
		error = np.sum(np.abs(error_vector))

		# Advance to the next command or land if we're done
		if error <= error_threshold:
			command_index = command_index + 1
			if len(input_commands) <= command_index:
				# Land
				break
			else:
				print("-- Next command")
				setpoint_vector = position_vector + input_commands[command_index]

		# Send position error values to drone
		await drone.offboard.set_position_ned(PositionNedYaw(
				error_vector[0], 
				error_vector[1],
				error_vector[2],
				0.0))

		await asyncio.sleep(offboard_frequency/1000)

	print("-- Landing")
	try:
		await drone.action.land()
	except ActionError as error:
		print(f"Landing failed with error code: {error._result.result}")

if __name__ == "__main__":
	loop = asyncio.get_event_loop()
	loop.run_until_complete(run())